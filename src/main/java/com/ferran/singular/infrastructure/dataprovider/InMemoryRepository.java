package com.ferran.singular.infrastructure.dataprovider;

import com.ferran.singular.application.AnimalRepository;
import com.ferran.singular.application.exception.UnsupportedAnimalException;
import com.ferran.singular.domain.Animal;
import com.ferran.singular.domain.ChickenAnimal;
import com.ferran.singular.domain.DogAnimal;
import com.ferran.singular.domain.ParrotAnimal;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryRepository implements AnimalRepository {
    private final Map<String, Animal> repository = new HashMap<>();

    public InMemoryRepository(String path) throws IOException, UnsupportedAnimalException {
        Reader in = new FileReader(path);
        Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
        for (CSVRecord entry : records) {
            Animal animalEntry;
            switch (entry.get("animalType")) {
                case "dog":
                    animalEntry = new DogAnimal(entry.get("name"), entry.get("favoriteFood"), entry.get("dogType"));
                    break;
                case "chicken":
                    String chickenWingLengthString = entry.get("wingLength");
                    double chickenWingLength = Double.parseDouble(chickenWingLengthString);
                    String broilerString = entry.get("broiler");
                    Boolean broiler = Boolean.parseBoolean(broilerString);
                    animalEntry = new ChickenAnimal(entry.get("name"), entry.get("favoriteFood"), chickenWingLength, broiler);
                    break;
                case "parrot":
                    String parrotWingLengthString = entry.get("wingLength");
                    double parrotWingLength = Double.parseDouble(parrotWingLengthString);
                    String talkString = entry.get("broiler");
                    Boolean talk = Boolean.parseBoolean(talkString);
                    animalEntry = new ParrotAnimal(entry.get("name"), entry.get("favoriteFood"), parrotWingLength, talk);
                    break;
                default:
                    throw new UnsupportedAnimalException("Animal type: " + entry.get("animalType") + " is unsupported");
            }
            repository.put(animalEntry.getName(), animalEntry);
        }
    }

    @Override
    public void persist(Animal animalEntry) {
        repository.put(animalEntry.getName(), animalEntry);
    }

    @Override
    public Animal retrieve(String animalName) {
        return repository.get(animalName);
    }

    @Override
    public List<Animal> retrieveAll() {
        List<Animal> animalList = new ArrayList<>();
        for (String key : repository.keySet()) {
            animalList.add(repository.get(key));
        }
        return animalList;
    }
}
