package com.ferran.singular.infrastructure.dataprovider;

import com.ferran.singular.application.LoggingService;

public class MuteLoggingService implements LoggingService {
    @Override
    public void log(String message) {

    }
}
