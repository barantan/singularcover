package com.ferran.singular.infrastructure.dataprovider;

import com.ferran.singular.application.MessageSender;
import org.beryx.textio.TextIO;
import org.beryx.textio.TextTerminal;

public class ConsoleSender implements MessageSender {
    private final TextIO textIO;

    public ConsoleSender(TextIO textIO) {
        this.textIO = textIO;
    }

    @Override
    public void send(String message) {
        TextTerminal terminal = textIO.getTextTerminal();
        terminal.print(message);
    }
}
