package com.ferran.singular.infrastructure.dataprovider;

import com.ferran.singular.application.RandomGeneratorService;

public class MathRandomGenerator implements RandomGeneratorService {
    @Override
    public int randomNumber(int maxValue) {
        if (maxValue == 0) {
            return 0;
        }

        double random = Math.random();
        double exponent = Math.nextUp(Math.log(maxValue));

        double doubleRandom = (Math.pow(10.0, exponent) * random) % maxValue;
        return (int) doubleRandom;
    }
}
