package com.ferran.singular.infrastructure.entrypoint;

import com.ferran.singular.application.AnimalRepository;
import com.ferran.singular.application.LoggingService;
import com.ferran.singular.application.MessageSender;
import com.ferran.singular.application.RandomGeneratorService;
import com.ferran.singular.application.exception.UnsupportedAnimalException;
import com.ferran.singular.application.usecase.ListAnimalsUseCase;
import com.ferran.singular.application.usecase.LiveADayUseCase;
import com.ferran.singular.domain.Animal;
import com.ferran.singular.infrastructure.dataprovider.ConsoleSender;
import com.ferran.singular.infrastructure.dataprovider.InMemoryRepository;
import com.ferran.singular.infrastructure.dataprovider.MathRandomGenerator;
import com.ferran.singular.infrastructure.dataprovider.MuteLoggingService;
import org.beryx.textio.TextIO;
import org.beryx.textio.TextIoFactory;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;

public class App {

    private static LoggingService logger;
    private static AnimalRepository repository;
    private static MessageSender sender;
    private static RandomGeneratorService randomGenerator;

    public static void main(String[] args) {
        try {
            TextIO textIO = TextIoFactory.getTextIO();

            String path;
            if (args.length == 0) {
                URL url = Thread.currentThread().getContextClassLoader().getResource("animals.csv");
                path = url.getFile();
            } else {
                path = args[0];
            }
            loadInitialState(textIO, path);

            showOptions(textIO);

            System.exit(0);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.exit(-1);
        }
    }

    private static void loadInitialState(TextIO textIO, String path) throws IOException, UnsupportedAnimalException {
        logger = new MuteLoggingService();
        repository = new InMemoryRepository(path);
        randomGenerator = new MathRandomGenerator();
        sender = new ConsoleSender(textIO);
    }

    private static void showOptions(TextIO textIO) {
        boolean endApp = false;

        while (!endApp) {
            AppOptions month = textIO.newEnumInputReader(AppOptions.class)
                    .read("What option do tou want to execute?");

            switch (month) {
                case LIST_ALL_ANIMALS:
                    ListAnimalsUseCase listAnimalsUseCase = new ListAnimalsUseCase(
                            logger,
                            repository
                    );
                    Collection<Animal> animalCollection = listAnimalsUseCase.invoke();
                    for (Animal animal : animalCollection) {
                        sender.send(animal.toString() + "\n");
                    }
                    break;

                case LIVE_A_DAY:
                    LiveADayUseCase liveADayUseCase = new LiveADayUseCase(
                            logger,
                            repository,
                            randomGenerator,
                            sender
                    );

                    liveADayUseCase.invoke();
                    break;

                case EXIT:
                    endApp = true;
                    break;

                default:
                    System.exit(-1);
                    break;
            }

            sender.send("\n");
        }
    }
}
