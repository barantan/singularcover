package com.ferran.singular.infrastructure.entrypoint;

public enum AppOptions {
    LIST_ALL_ANIMALS("List all animals"),
    LIVE_A_DAY("Live a day"),
    EXIT("Close application");

    private String description;

    AppOptions(String description) {
        this.description = description;
    }

    public String description() {
        return description;
    }

    @Override
    public String toString() {
        return description();
    }
}
