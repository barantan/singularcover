package com.ferran.singular.application;

public interface LoggingService {
    void log(String message);
}
