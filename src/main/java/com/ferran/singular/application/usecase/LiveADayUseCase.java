package com.ferran.singular.application.usecase;

import com.ferran.singular.application.AnimalRepository;
import com.ferran.singular.application.LoggingService;
import com.ferran.singular.application.MessageSender;
import com.ferran.singular.application.RandomGeneratorService;
import com.ferran.singular.domain.Animal;

import java.util.List;

public class LiveADayUseCase {
    private final LoggingService logger;
    private final AnimalRepository animalRepository;
    private final RandomGeneratorService randomGenerator;
    private final MessageSender messageSender;

    public LiveADayUseCase(
            LoggingService logger,
            AnimalRepository animalRepository,
            RandomGeneratorService randomGenerator,
            MessageSender messageSender
    ) {

        this.logger = logger;
        this.animalRepository = animalRepository;
        this.randomGenerator = randomGenerator;
        this.messageSender = messageSender;
    }

    public void invoke() {
        logger.log("Start " + getClass().getName());

        List<Animal> animals = animalRepository.retrieveAll();
        for (Animal animal : animals) {
            removeFriendship(animal);
            addFriendship(animal, animals);
        }

        logger.log("End " + getClass().getName());
    }

    private void addFriendship(Animal animal, List<Animal> animals) {
        int chosenFriend = randomGenerator.randomNumber(animals.size() - 2);

        Animal newFriend = animals.get(chosenFriend);
        if (newFriend.getName().equals(animal.getName())) {
            newFriend = animals.get(chosenFriend + 1);
        }

        messageSender.send(animal.getName() + " gets " + newFriend.getName() + " as a friend \n");
        animal.addFriend(newFriend.getName());
        newFriend.addFriend(animal.getName());

        animalRepository.persist(newFriend);
        animalRepository.persist(animal);
    }

    private void removeFriendship(Animal animal) {
        int friendSize = animal.getFriendsIds().size();
        if (friendSize > 0) {
            int positionToRemove = randomGenerator.randomNumber(friendSize - 1);
            String[] friendArray = new String[0];
            friendArray = animal.getFriendsIds().toArray(friendArray);
            String friendToRemove = friendArray[positionToRemove];

            Animal exFriend = animalRepository.retrieve(friendToRemove);

            messageSender.send(animal.getName() + " loses " + exFriend.getName() + " as a friend \n");
            animal.removeFriend(friendToRemove);
            exFriend.removeFriend(animal.getName());

            animalRepository.persist(exFriend);
            animalRepository.persist(animal);
        }
    }
}
