package com.ferran.singular.application.usecase;

import com.ferran.singular.application.AnimalRepository;
import com.ferran.singular.application.LoggingService;
import com.ferran.singular.domain.Animal;

import java.util.Collection;
import java.util.List;

public class ListAnimalsUseCase {
    private final LoggingService logger;
    private final AnimalRepository animalRepository;

    public ListAnimalsUseCase(
            LoggingService logger,
            AnimalRepository animalRepository
    ) {

        this.logger = logger;
        this.animalRepository = animalRepository;
    }

    public Collection<Animal> invoke() {
        logger.log("Start " + getClass().getName());

        List<Animal> animals = animalRepository.retrieveAll();

        logger.log("End " + getClass().getName());
        return animals;
    }
}
