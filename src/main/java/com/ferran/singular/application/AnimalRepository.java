package com.ferran.singular.application;

import com.ferran.singular.domain.Animal;

import java.util.List;

public interface AnimalRepository {
    void persist(Animal animalEntry);

    Animal retrieve(String animalName);

    List<Animal> retrieveAll();
}
