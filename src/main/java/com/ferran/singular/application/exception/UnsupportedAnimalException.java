package com.ferran.singular.application.exception;

public class UnsupportedAnimalException extends Exception {
    public UnsupportedAnimalException() {
    }

    public UnsupportedAnimalException(String message) {
        super(message);
    }

    public UnsupportedAnimalException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedAnimalException(Throwable cause) {
        super(cause);
    }

    public UnsupportedAnimalException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
