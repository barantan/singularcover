package com.ferran.singular.application;

public interface RandomGeneratorService {
    int randomNumber(int maxValue);
}
