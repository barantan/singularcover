package com.ferran.singular.application;

public interface MessageSender {
    void send(String message);
}
