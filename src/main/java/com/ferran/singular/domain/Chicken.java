package com.ferran.singular.domain;

public interface Chicken {
    Boolean isBroiler();
}
