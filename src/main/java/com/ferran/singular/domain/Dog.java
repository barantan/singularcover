package com.ferran.singular.domain;

public interface Dog {
    String getDogType();
}
