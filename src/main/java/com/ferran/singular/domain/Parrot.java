package com.ferran.singular.domain;

public interface Parrot {
    Boolean canTalk();
}
