package com.ferran.singular.domain;

public interface Bird {
    Double getWingLength();
}
