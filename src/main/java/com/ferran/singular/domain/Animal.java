package com.ferran.singular.domain;

import java.util.HashSet;
import java.util.Set;

public class Animal {
    private final String name;
    private final String favoriteFood;
    private final Set<String> friendsIds;

    public Animal(String name, String favoriteFood) {
        this(name, favoriteFood, new HashSet<>());
    }

    public Animal(String name, String favoriteFood, Set<String> friendsIds) {
        this.name = name;
        this.favoriteFood = favoriteFood;
        this.friendsIds = friendsIds;
    }

    public String getName() {
        return name;
    }

    public String getFavoriteFood() {
        return favoriteFood;
    }

    public Set<String> getFriendsIds() {
        return friendsIds;
    }

    public void addFriend(String name) {
        friendsIds.add(name);
    }

    public void removeFriend(String friendToRemove) {
        friendsIds.remove(friendToRemove);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                "name='" + name + '\'' +
                ", favoriteFood='" + favoriteFood + '\'' +
                ", friendsIds=" + friendsIds +
                '}';
    }
}
