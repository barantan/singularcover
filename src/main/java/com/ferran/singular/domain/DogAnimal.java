package com.ferran.singular.domain;

import java.util.Set;

public class DogAnimal extends Animal implements Dog {
    private final String dogType;

    public DogAnimal(String name, String favoriteFood, Set<String> friends, String dogType) {
        super(name, favoriteFood, friends);
        this.dogType = dogType;
    }

    public DogAnimal(String name, String favoriteFood, String dogType) {
        super(name, favoriteFood);
        this.dogType = dogType;
    }

    @Override
    public String getDogType() {
        return this.dogType;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                "name='" + getName() + '\'' +
                ", favoriteFood='" + getFavoriteFood() + '\'' +
                ", friendsIds=" + getFriendsIds() +
                ", dogType='" + dogType + '\'' +
                '}';
    }
}
