package com.ferran.singular.domain;

import java.util.Set;

public class ParrotAnimal extends Animal implements Bird, Parrot {
    private final Double wingLength;
    private final Boolean talk;

    public ParrotAnimal(String name, String favoriteFood, Set<String> friendsIds, Double wingLength, Boolean talk) {
        super(name, favoriteFood, friendsIds);
        this.wingLength = wingLength;
        this.talk = talk;
    }

    public ParrotAnimal(String name, String favoriteFood, Double wingLength, Boolean talk) {
        super(name, favoriteFood);
        this.wingLength = wingLength;
        this.talk = talk;
    }

    @Override
    public Double getWingLength() {
        return this.wingLength;
    }

    @Override
    public Boolean canTalk() {
        return this.talk;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                "name='" + getName() + '\'' +
                ", favoriteFood='" + getFavoriteFood() + '\'' +
                ", friendsIds=" + getFriendsIds() +
                ", wingLength=" + wingLength +
                ", talk=" + talk +
                '}';
    }
}
