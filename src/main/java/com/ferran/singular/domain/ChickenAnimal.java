package com.ferran.singular.domain;

import java.util.Set;

public class ChickenAnimal extends Animal implements Bird, Chicken {
    private final Double wingLength;
    private final Boolean broiler;

    public ChickenAnimal(String name, String favoriteFood, Set<String> friendsIds, Double wingLength, Boolean broiler) {
        super(name, favoriteFood, friendsIds);
        this.wingLength = wingLength;
        this.broiler = broiler;
    }

    public ChickenAnimal(String name, String favoriteFood, Double wingLength, Boolean broiler) {
        super(name, favoriteFood);
        this.wingLength = wingLength;
        this.broiler = broiler;
    }

    @Override
    public Double getWingLength() {
        return wingLength;
    }

    @Override
    public Boolean isBroiler() {
        return broiler;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " {" +
                "name='" + getName() + '\'' +
                ", favoriteFood='" + getFavoriteFood() + '\'' +
                ", friendsIds=" + getFriendsIds() +
                ", wingLength=" + wingLength +
                ", broiler=" + broiler +
                '}';
    }
}
