package stepDefinitions;

import com.ferran.singular.application.AnimalRepository;
import com.ferran.singular.application.LoggingService;
import com.ferran.singular.application.MessageSender;
import com.ferran.singular.application.RandomGeneratorService;
import com.ferran.singular.application.exception.UnsupportedAnimalException;
import com.ferran.singular.application.usecase.ListAnimalsUseCase;
import com.ferran.singular.application.usecase.LiveADayUseCase;
import com.ferran.singular.domain.Animal;
import com.ferran.singular.domain.ChickenAnimal;
import com.ferran.singular.domain.DogAnimal;
import com.ferran.singular.domain.ParrotAnimal;
import com.ferran.singular.infrastructure.*;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.*;

public class MyStepdefs {

    private LoggingService logger;
    private AnimalRepository animalRepository;
    private ListAnimalsUseCase listAnimalsUseCase;
    private Collection<Animal> animalsList;
    private RandomGeneratorService randomGenerator;
    private LiveADayUseCase liveADayUseCase;
    private MessageSender messageSender;

    @Given("A logging service")
    public void initializeLoggingService() {
        logger = new InternalLog();
    }

    @Given("An Animal repository connection")
    public void initializeAnimalRepositoryConnection() {
        animalRepository = new MockAnimalRepository();
    }

    @Given("The Animal repository is empty")
    public void initializeEmptyAnimalRepository() {

    }

    @Given("A list Animals use case")
    public void initializeListAnimalsUseCase() {
        listAnimalsUseCase = new ListAnimalsUseCase(
                logger,
                animalRepository
        );
    }

    @When("I invoke the list Animals use case")
    public void invokeListAnimalsUseCase() {
        animalsList = listAnimalsUseCase.invoke();
    }

    @Then("I get an empty list of Animals")
    public void checkEmptyListOfAnimals() {
        Assert.assertEquals(0, animalsList.size());
    }

    @Given("The Animal repository has the entries:")
    public void initializeAnimalRepositoryEntries(List<CucumberAnimalRow> data) throws UnsupportedAnimalException {
        fillAnimalRepository(data);
    }

    private Set<String> parseFriends(String friends) {
        if (friends != null) {
            String[] friendsArray = friends.split(",");
            return new HashSet<>(Arrays.asList(friendsArray));
        } else {
            return new HashSet<>();
        }
    }

    @Then("^I get a list of Animals with (\\d+) listed animals$")
    public void checkListAnimalsSize(int expectedSize) {
        Assert.assertNotNull(animalsList);
        Assert.assertEquals(expectedSize, animalsList.size());
    }

    @Then("^There is listed an animal with name \"([^\"]*)\"$")
    public void checkThereIsListedAnimal(String name) {
        Animal animal = animalRepository.retrieve(name);

        Assert.assertNotNull(animal);
        Assert.assertEquals(name, animal.getName());
    }

    @Given("^An initial state of the animals$")
    public void initializeAnimalsState(List<CucumberAnimalRow> data) throws UnsupportedAnimalException {
        fillAnimalRepository(data);
    }

    private void fillAnimalRepository(List<CucumberAnimalRow> data) throws UnsupportedAnimalException {
        for (CucumberAnimalRow entry : data) {
            final Animal animalEntry;
            final Set<String> entryFriends = parseFriends(entry.getFriends());
            switch (entry.getAnimalType()) {
                case "dog":
                    animalEntry = new DogAnimal(entry.getName(), entry.getFavoriteFood(), entryFriends, entry.getDogType());
                    break;
                case "chicken":
                    animalEntry = new ChickenAnimal(entry.getName(), entry.getFavoriteFood(), entryFriends, entry.getWingLength(), entry.getBroiler());
                    break;
                case "parrot":
                    animalEntry = new ParrotAnimal(entry.getName(), entry.getFavoriteFood(), entryFriends, entry.getWingLength(), entry.getTalk());
                    break;
                default:
                    throw new UnsupportedAnimalException("Animal type: " + entry.getAnimalType() + " is unsupported");
            }

            animalRepository.persist(animalEntry);
        }
    }

    @Given("^A mock random generator with seed \"([^\"]*)\"$")
    public void initializeMockRandomGeneratorWithSeed(String randomSeed) {
        randomGenerator = new MockRandomGenerator(randomSeed);
    }

    @Given("^A live a day use case$")
    public void initializeLiveADayUseCase() {
        liveADayUseCase = new LiveADayUseCase(
                logger,
                animalRepository,
                randomGenerator,
                messageSender
        );
    }

    @When("^I invoke the live a day use case$")
    public void invokeLiveADayUseCase() {
        liveADayUseCase.invoke();
    }

    @Then("^The Animal \"([^\"]*)\" has friendship with \"([^\"]*)\"$")
    public void theAnimalHasFriendshipWith(String animalId, String expectedFriend) {
        Animal animal = animalRepository.retrieve(animalId);

        Assert.assertNotNull(animal);
        Assert.assertTrue(animal.getFriendsIds().contains(expectedFriend));
    }

    @Given("^An output for messages to be sent$")
    public void initializeOutputForMessagesToBeSent() {
        messageSender = new MockMessageSender();
    }
}
