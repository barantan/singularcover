package com.ferran.singular.infrastructure;

import com.ferran.singular.application.LoggingService;

public class InternalLog implements LoggingService {
    @Override
    public void log(String message) {
        System.out.println(message);
    }
}
