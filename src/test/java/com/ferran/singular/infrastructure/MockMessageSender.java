package com.ferran.singular.infrastructure;

import com.ferran.singular.application.MessageSender;

public class MockMessageSender implements MessageSender {
    @Override
    public void send(String message) {
        System.out.println(message);
    }
}
