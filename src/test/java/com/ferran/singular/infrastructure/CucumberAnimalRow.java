package com.ferran.singular.infrastructure;

public class CucumberAnimalRow {
    private final String name;
    private final String animalType;
    private final String friends;
    private final String favoriteFood;
    private final String dogType;
    private final Double wingLength;
    private final Boolean talk;
    private final Boolean broiler;

    public CucumberAnimalRow(
            String name, String animalType, String friends, String favoriteFood, String dogType,
            Double wingLength, Boolean talk, Boolean broiler
    ) {
        this.name = name;
        this.animalType = animalType;
        this.friends = friends;
        this.favoriteFood = favoriteFood;
        this.dogType = dogType;
        this.wingLength = wingLength;
        this.talk = talk;
        this.broiler = broiler;
    }

    public String getName() {
        return name;
    }

    public String getAnimalType() {
        return animalType;
    }

    public String getFriends() {
        return friends;
    }

    public String getFavoriteFood() {
        return favoriteFood;
    }

    public String getDogType() {
        return dogType;
    }

    public Double getWingLength() {
        return wingLength;
    }

    public Boolean getTalk() {
        return talk;
    }

    public Boolean getBroiler() {
        return broiler;
    }
}
