package com.ferran.singular.infrastructure;

import com.ferran.singular.application.AnimalRepository;
import com.ferran.singular.domain.Animal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MockAnimalRepository implements AnimalRepository {
    private final Map<String, Animal> repository = new HashMap<>();

    @Override
    public void persist(Animal animalEntry) {
        repository.put(animalEntry.getName(), animalEntry);
    }

    @Override
    public Animal retrieve(String animalName) {
        return repository.get(animalName);
    }

    @Override
    public List<Animal> retrieveAll() {
        List<Animal> animalList = new ArrayList<>();
        for (String key : repository.keySet()) {
            animalList.add(repository.get(key));
        }
        return animalList;
    }
}
