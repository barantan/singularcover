package com.ferran.singular.infrastructure;

import com.ferran.singular.application.RandomGeneratorService;

public class MockRandomGenerator implements RandomGeneratorService {

    private final Integer value;

    public MockRandomGenerator(String randomSeed) {
        value = Integer.parseInt(randomSeed);
    }

    @Override
    public int randomNumber(int maxValue) {
        if (maxValue == 0) {
            return 0;
        }
        return value % maxValue;
    }
}
