Feature: List all animals with their properties and friends’ names

  Scenario: Empty list of animals
    Given A logging service
    And An Animal repository connection
    And The Animal repository is empty
    And A list Animals use case
    When I invoke the list Animals use case
    Then I get an empty list of Animals

  Scenario Outline: List has some animals
    Given A logging service
    And An Animal repository connection
    And The Animal repository has the entries:
      | name      | animalType   | friends   | favoriteFood   | dogType    | wingLength   | talk   | broiler   |
      | Dog 1     | dog          | Parrot 1  | meat           | hunter dog |              |        |           |
      | Chicken 1 | chicken      | none      | corn           |            | 3.12         |        | false     |
      | Parrot 1  | parrot       | Dog 1     | lettuce        |            | 1.25         | true   |           |
      | <name>    | <animalType> | <friends> | <favoriteFood> | <dogType>  | <wingLength> | <talk> | <broiler> |
    And A list Animals use case
    When I invoke the list Animals use case
    Then I get a list of Animals with 4 listed animals
    And There is listed an animal with name "<name>"

    Examples:
      | name  | animalType | friends | favoriteFood | dogType  | wingLength | talk | broiler |
      | Dog 2 | dog        | Dog 1   | chicken      | home dog |            |      |         |