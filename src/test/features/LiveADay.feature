Feature: Live a day

  Scenario: Living a day
    Given A logging service
    And An Animal repository connection
    And An initial state of the animals
      | name      | animalType | favoriteFood | dogType    | wingLength | talk | broiler |
      | Dog 1     | dog        | meat         | hunter dog |            |      |         |
      | Chicken 1 | chicken    | corn         |            | 3.12       |      | false   |
      | Parrot 1  | parrot     | lettuce      |            | 1.25       | true |         |
    And A mock random generator with seed "1"
    And An output for messages to be sent
    And A live a day use case
    When I invoke the live a day use case
    Then The Animal "Dog 1" has friendship with "Chicken 1"
    Then The Animal "Chicken 1" has friendship with "Dog 1"
    Then The Animal "Parrot 1" has friendship with "Chicken 1"