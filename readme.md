# Readme
The main class can be found inside the package com.ferran.singular.infrastructure.entrypoint.App 

It's possible to pass a file path as an argument to initialize the animal repository, the expected file is CSV using the same format as the file used by default. The default file used can be found inside the resources folder with the name "animals.csv".

To allow a quick run from the source code use the following command from the terminal "mvn package exec:java" this will compile, execute existing tests and run the application with the default animal repository values.